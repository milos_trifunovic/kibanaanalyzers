using KibanaViewStateAnalyzer;
using KibanaViewStateAnalyzer.Subgroups;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KKibanaViewStateTests
{
	[TestClass]
	public class UnitTest1
	{
		private ViewStateLogAnalyzer _analyzer;

		[TestInitialize]
		public void Initialize()
		{
			_analyzer = new ViewStateLogAnalyzer();
		}
		[TestMethod]
		public void TestMethod1()
		{
			var message = "Load view state for file \"{dsakdsajkl}\" size 2,455,741, time: 1.45 sec, lock time: 1 sec, serialize time: 0.4 sec";
			var data = _analyzer.Analyze(message);

			Assert.IsNotNull(data);
			Assert.AreEqual(Operation.Load, data.Operation);
			Assert.AreEqual(1.45m, data.TotalTime);
			Assert.AreEqual(1m, data.LockTime);
			Assert.AreEqual(0.4m, data.SerializeTime);
		}

		[TestMethod]
		public void TestCopySubgroupTimes()
		{
			var message = "Copy subgroup 8617726 departure 01/11/2020 Unit/Total 104/104- copy subgroup 462ms, recalculate 14969ms, save 4681ms, PFO 0ms, Total 20112ms";

			var parsed = CopySubgroupTime.ParseString(message);
			
		}
	}
}
