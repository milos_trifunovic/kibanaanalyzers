﻿using System;
using System.Linq;
using KibanaViewStateAnalyzer.FinancialReportsAnalyzers.Services;

namespace KibanaViewStateAnalyzer
{
	class Program
    {
		static AsianTrailsLogFinder logFinder = new AsianTrailsLogFinder();
		static ViewStateLogAnalyzer analyzer = new ViewStateLogAnalyzer();

		static FInancialReportLogFinder reportFinder = new FInancialReportLogFinder();
		static FinancialReportAnalyzer reportAnalyzer = new FinancialReportAnalyzer();
		static void Main(string[] args)
		{
			var instanceName = "asian-trails";
			if (args.Length == 0)
			{
				Console.WriteLine("Please provide instance name as parameter");
				Console.WriteLine("KibanaViewStateAnalyzer.exe asian-trails");
				Console.WriteLine("Defaulting to asian-trails");
			}
			else
			{
				instanceName = args[0];
			}
			var startDate = new DateTime(2019, 12, 1);
			var endDate = DateTime.Now;

			var reports = reportFinder.GetMessages(instanceName, startDate, endDate);
			var response = reportAnalyzer.AnalyzeReport(reports, instanceName);
			Console.WriteLine(response);
		}

		private static void GetCopySubgroupStatistics(DateTime dateFrom, DateTime dateTo)
		{
			var logs = logFinder.GetSubgroupCopyTimesMessages(dateFrom, dateTo);
			var allLogs = string.Join("\n", logs);
			var totalLogs = logs.Count();
		}

		private static void GetAndFormatLogs(DateTime dateFrom, DateTime dateTo)
		{
			var logs = logFinder.GetViewStateMessages(dateFrom, dateTo);
			var allLogs = string.Join("\n", logs);
			var totalLogs = logs.Count();
			Console.WriteLine($"Total logs: {totalLogs}");
			var analizedLogs = analyzer.AnalyzeLogData(logs);
			foreach (var item in analizedLogs)
			{
				Console.WriteLine($"Operation {item.Operation}, total time: {Math.Round(item.TotalTime, 3)} sec, lock time: {Math.Round(item.LockTime, 3)} sec, serialize time: {Math.Round(item.SerializeTime, 3)}");
			}
		}
	}
}
