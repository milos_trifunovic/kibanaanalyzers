﻿namespace KibanaViewStateAnalyzer.Subgroups
{
	public class CopySubgroupTime
	{
		public int SubgroupID { get; set; }
		public long CopySubgroup { get; set; }
		public long Recalculate { get; set; }
		public long Save { get; set; }
		public long PFO { get; set; }
		public long Total { get; set; }
		public int TotalUnits { get; set; }
		public int UnitCopied { get; set; }

		public static CopySubgroupTime ParseString(string source)
		{
			// Copy subgroup 8617726 departure 01/11/2020 Unit/Total 104/104- copy subgroup 462ms, recalculate 14969ms, save 4681ms, PFO 0ms, Total 20112ms
			if (source == null || source == string.Empty)
			{
				return null;
			}

			var parameters = source.Split(' ');
			var entry = new CopySubgroupTime();

			entry.SubgroupID = int.Parse(parameters[2]);

			var copiedTotal = parameters[6].Split('/');
			var first = copiedTotal[0];
			var second = copiedTotal[1].Replace("-", "");
			entry.TotalUnits = int.Parse(second);
			entry.UnitCopied = int.Parse(first);

			entry.CopySubgroup = GetFormattedMilliseconds(parameters, 9);
			entry.Recalculate = GetFormattedMilliseconds(parameters, 11);
			entry.Save = GetFormattedMilliseconds(parameters, 13);
			entry.PFO = GetFormattedMilliseconds(parameters, 15);
			entry.Total = GetFormattedMilliseconds(parameters, 17);

			return entry;
		}

		private static long GetFormattedMilliseconds(string[] parameters, int index)
		{
			var value = parameters[index].Replace("ms", "");
			value = value.Replace(",", "");
			var x = long.Parse(value);
			return x;
		}
	}
}
