﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nest;

namespace KibanaViewStateAnalyzer
{
	public class LogFinder
	{
		const string KIBANA_URL = "https://5787cc8837bab7e8f5562df6e86cd816.us-east-1.aws.found.io:9243";
		const string KIBANA_INDEX = "lemax-app-*";
		const string KIBANA_USERNAME = "lemax-app";
		const string KIBANA_PASSWORD = "upMVer6t6NBts7XU";

		public IEnumerable<string> GetMessages(string logFolderName, string instanceName, DateTime from, DateTime to)
		{
			var client = CreateClient();

			var results = client
				.Search<EventLog>(arg => GetRawQuery(arg, logFolderName, instanceName, from, to)
				.Aggregations(log => log
					.Terms("LogFolderName", t => t
						.Size(int.MaxValue)
							.Field(l => l.Message.Suffix("keyword"))
						)
				));
			var buckets = results.Aggregations.Terms("LogFolderName").Buckets;

			var response = buckets.Select(x => x.Key).AsEnumerable();

			return response;
		}

		protected SearchDescriptor<EventLog> GetRawQuery(SearchDescriptor<EventLog> arg, string logFolderName, string instanceName, DateTime from, DateTime to)
		{
			return arg
				.Query(q => q
					.DateRange(d => d
						.Field(ev => ev.Time)
						.GreaterThanOrEquals(from)
						.LessThanOrEquals(to))
					&& q.Exists(e => e.Field(log => log.Message))
					&& q.Match(t => t.Field(f => f.LogFolderName).Query(logFolderName))
					&& q.Match(t => t.Field(f => f.InstanceName).Query(instanceName))
				);
		}

		protected ElasticClient CreateClient()
		{
			var url = new Uri(KIBANA_URL);
			var settings = new ConnectionSettings(url)
				.RequestTimeout(TimeSpan.FromSeconds(180))
				.DefaultMappingFor<EventLog>(l => l
					.IndexName(KIBANA_INDEX))
				.BasicAuthentication(KIBANA_USERNAME, KIBANA_PASSWORD)
				.EnableDebugMode();
			settings.DefaultFieldNameInferrer(t => t);
			var client = new ElasticClient(settings);
			return client;
		}
	}
	class AsianTrailsLogFinder : LogFinder
	{
		public IEnumerable<string> GetViewStateMessages(DateTime from, DateTime to)
		{
			return GetMessages("ViewState", "asian-trails", from, to);
		}

		public IEnumerable<string> GetSubgroupCopyTimesMessages(DateTime from, DateTime to)
		{
			return GetMessages("SubgroupCopyTimes", "asian-trails", from, to);
		}
	}
}
