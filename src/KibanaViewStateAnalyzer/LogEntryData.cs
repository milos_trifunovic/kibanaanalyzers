﻿namespace KibanaViewStateAnalyzer
{
	public class LogEntryData
	{
		public LogEntryData(Operation operation, decimal totalTime, decimal lockTime, decimal serializeTime)
		{
			Operation = operation;
			TotalTime = totalTime;
			LockTime = lockTime;
			SerializeTime = serializeTime;
		}

		public decimal TotalTime { get; set; }
		public decimal LockTime { get; set; }
		public decimal SerializeTime { get; set; }
		public Operation Operation { get; set; }
	}

	public enum Operation { None, Load, Save }
}
