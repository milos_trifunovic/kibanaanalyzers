﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace KibanaViewStateAnalyzer
{
	public class ViewStateLogAnalyzer
	{
		private static NumberStyles _style = NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands;
		private static CultureInfo _provider = new CultureInfo("en-US");

		public IEnumerable<LogEntryData> AnalyzeLogData(IEnumerable<string> messages)
		{
			//var logs = new List<LogEntryData>();
			//foreach (var message in messages)
			//{
			//	var entry = Analyze(message);
			//	if (entry != null)
			//	{
			//		logs.Add(entry);
			//	}
			//}

			//var groupedLogs = logs
			//	.Where(x => x != null)
			//	.GroupBy(x => x.Operation)
			//	.ToDictionary(x => x.Key, x => x.ToList());

			//var toReturn = new List<LogEntryData>();
			//foreach (var item in groupedLogs)
			//{
			//	var total = item.Value.Sum(x => x.TotalTime);
			//	var lockTime = item.Value.Sum(x => x.LockTime);
			//	var serializeTime = item.Value.Sum(x => x.SerializeTime);
			//	var count = item.Value.Count;

			//	var lockeditems = item.Value.Where(x => x.LockTime > 0).OrderByDescending(x => x.LockTime).ToList();

			//	var entry = new LogEntryData(item.Key, total / count, lockTime / count, serializeTime / count);
			//	toReturn.Add(entry);
			//}

			//return toReturn;

			return messages
				.Select(x => Analyze(x))
				.Where(x => x != null)
				.GroupBy(x => x.Operation)
				.Select(x => new
				{
					Operation = x.Key,
					AverageTotalTime = x.Average(y => y.TotalTime),
					AverageLockTime = x.Average(y => y.LockTime),
					AverageSerializeTime = x.Average(y => y.SerializeTime)
				})
				.Select(x => new LogEntryData(x.Operation, x.AverageTotalTime, x.AverageLockTime, x.AverageSerializeTime))
				.AsEnumerable();
		}

		public LogEntryData Analyze(string message)
		{
			if (string.IsNullOrWhiteSpace(message))
			{
				return null;
			}

			var tokens = message.Split(' ');
			var operation = GetOperation(tokens);
			var totalTime = GetTotalTime(tokens);
			var lockTime = GetLockTime(tokens);
			var serializationTime = GetSerializationTime(tokens);

			var response = new LogEntryData(operation, totalTime, lockTime, serializationTime);

			return response;
		}

		private Operation GetOperation(string[] tokens)
		{
			var operationString = tokens[0];
			if (operationString.ToLower() == Operation.Load.ToString().ToLower())
			{
				return Operation.Load;
			}
			if (operationString.ToLower() == Operation.Save.ToString().ToLower())
			{
				return Operation.Save;
			}

			return Operation.None;
		}

		private decimal GetTotalTime(string[] tokens)
		{
			var searchString = "time:";
			int position = GetPosition(tokens, searchString);

			if (position > -1)
			{
				var time = tokens[position + 1];
				
				return ParseDecimal(time);
			}

			return -1m;
		}

		private static decimal ParseDecimal(string time)
		{
			var number = Decimal.Parse(time, _style, _provider);

			return number;
		}

		private decimal GetLockTime(string[] tokens)
		{
			var searchString = "lock";
			int position = GetPosition(tokens, searchString);

			if (position > -1)
			{
				var time = tokens[position + 2];
				return ParseDecimal(time);
			}

			return -1m;
		}

		private decimal GetSerializationTime(string[] tokens)
		{
			var searchString = "serialize";
			int position = GetPosition(tokens, searchString);

			if (position > -1)
			{
				var time = tokens[position + 2];
				return ParseDecimal(time);
			}

			return -1m;
		}

		private int GetPosition(string[] tokens, string searchString)
		{
			var position = -1;
			for (int i = 0; i < tokens.Length; i++)
			{
				if (tokens[i] == searchString)
				{
					position = i;
					break;
				}
			}

			return position;
		}
	}
}
