﻿using System;

namespace KibanaViewStateAnalyzer
{
	//DTO for loading data from kibana.
	public class EventLog
	{
		public DateTime Time { get; set; }

		/// <summary>
		/// Source of the log type (LemaxLogTypeEnum).
		/// This field is mandatory.
		/// Values:
		/// - Log   - general logging (LogHelper)
		/// - Audit - audit logging (Dnevnik/Audit)
		/// - HME   - health monitoring
		/// - CRE   - critical error monitoring
		/// </summary>
		public string LogType { get; set; }
		public string InstanceName { get; set; }
		public string LogFolderName { get; set; }

		/// <summary>
		/// Critical error code.
		/// Mandatory if the log type is CRE.
		/// </summary>
		public int? CriticalErrorCode { get; set; }

		/// <summary>
		/// A hashed string that defines one error occurence - used by error monitoring tools to group instances (occurrences) of the same error into one entry
		/// - for Web errors: stack trace
		/// - for JS errors: error message
		/// Mandatory if the log type is HME.
		/// </summary>
		public string ErrorGroupingKey { get; set; }

		public string EnvironmentType { get; set; }

		public string Message { get; set; }
		public string RequestGuid { get; set; }

		public override string ToString()
		{
			return $"{Time} {LogType} {ErrorGroupingKey}";
		}
	}
}
