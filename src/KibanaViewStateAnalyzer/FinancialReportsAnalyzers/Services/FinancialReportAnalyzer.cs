﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace KibanaViewStateAnalyzer.FinancialReportsAnalyzers.Services
{
	class FinancialReportAnalyzer
	{
		public string AnalyzeReport(IEnumerable<EventLog> logs, string instanceName)
		{
			var reportFIlters = new List<FinancialReportFilter>();

			var data = logs
				.Select(x => JsonConvert.DeserializeObject<FinancialReportFilter>(x.Message));

			var totalFilterHits = logs.Count();
			var properties = typeof(FinancialReportFilter).GetProperties();

			var analysis = new List<PropertyMessage>();
			analysis.Add(AnalyzeGroupingOptions(data));
			analysis.Add(AnalyzeReservationGrouping(data));
			analysis.Add(AnalyzeSetPaid(data));
			analysis.Add(AnalyzeReservationInCurrency(data));
			analysis.Add(AnalyzeReservationVatCalculationTypes(data));
			analysis.Add(AnalyzeReservationID(data));
			analysis.Add(AnalyzeStartPeriod(data));
			analysis.Add(AnalyzeEndPeriod(data));
			analysis.Add(AnalyzeBookingDateFrom(data));
			analysis.Add(AnalyzeBookingDateTo(data));
			analysis.Add(AnalyzeSupplierInvoiceExistsFrom(data));
			analysis.Add(AnalyzeSupplierInvoiceExistsTo(data));
			analysis.Add(AnalyzeSupplierInvoiceDateFrom(data));
			analysis.Add(AnalyzeSupplierInvoiceDateTo(data));
			analysis.Add(AnalyzeInvoiceDateFrom(data));
			analysis.Add(AnalyzeInvoiceDateTo(data));
			analysis.Add(AnalyzeReservationDateFrom(data));
			analysis.Add(AnalyzeReservationDateTo(data));
			analysis.Add(AnalyzeUnitTypeID(data));
			analysis.Add(AnalyzeListIsClosed(data));
			analysis.Add(AnalyzeCountryID(data));
			analysis.Add(AnalyzeCityID(data));
			analysis.Add(AnalyzeSupplierID(data));
			analysis.Add(AnalyzeCustomerID(data));
			analysis.Add(AnalyzeUserID(data));
			analysis.Add(AnalyzeSetReservationItemEmployeeIDs(data));
			analysis.Add(AnalyzeListServices(data));
			analysis.Add(AnalyzeListSeasons(data));
			analysis.Add(AnalyzeCustomFieldID(data));
			analysis.Add(AnalyzeListDepartments(data));
			analysis.Add(AnalyzeListGroupServices(data));
			analysis.Add(AnalyzeListCustomerTypes(data));
			analysis.Add(AnalyzeListUnitTypes(data));
			analysis.Add(AnalyzeDepartmentLevel(data));

			var responseDTO = new
			{ 
				InstanceName = instanceName,
				ReportUsage = analysis
			};
			var jsonResponse = JsonConvert.SerializeObject(responseDTO);

			return jsonResponse;
		}

		private PropertyMessage AnalyzeReservationVatCalculationTypes(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "ReservationVatCalculationTypes", x => x.ReservationVatCalculationTypes != null && x.ReservationVatCalculationTypes.Count > 0);
		}
		private PropertyMessage AnalyzeReservationID(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "ReservationID", x => x.ReservationID > 0);
		}
		private PropertyMessage AnalyzeStartPeriod(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "StartPeriod", x => x.StartPeriod.HasValue);
		}
		private PropertyMessage AnalyzeEndPeriod(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "EndPeriod", x => x.EndPeriod.HasValue);
		}
		private PropertyMessage AnalyzeBookingDateFrom(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "BookingDateFrom", x => x.BookingDateFrom.HasValue);
		}
		private PropertyMessage AnalyzeBookingDateTo(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "BookingDateTo", x => x.BookingDateTo.HasValue);
		}
		private PropertyMessage AnalyzeSupplierInvoiceExistsFrom(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "SupplierInvoiceExistsFrom", x => x.SupplierInvoiceExistsFrom.HasValue);
		}
		private PropertyMessage AnalyzeSupplierInvoiceExistsTo(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "SupplierInvoiceExistsTo", x => x.SupplierInvoiceExistsTo.HasValue);
		}
		private PropertyMessage AnalyzeSupplierInvoiceDateFrom(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "SupplierInvoiceDateFrom", x => x.SupplierInvoiceDateFrom.HasValue);
		}
		private PropertyMessage AnalyzeSupplierInvoiceDateTo(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "SupplierInvoiceDateTo", x => x.SupplierInvoiceDateTo.HasValue);
		}
		private PropertyMessage AnalyzeInvoiceDateFrom(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "InvoiceDateFrom", x => x.InvoiceDateFrom.HasValue);
		}
		private PropertyMessage AnalyzeInvoiceDateTo(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "InvoiceDateTo", x => x.InvoiceDateTo.HasValue);
		}
		private PropertyMessage AnalyzeReservationDateFrom(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "ReservationDateFrom", x => x.ReservationDateFrom.HasValue);
		}
		private PropertyMessage AnalyzeReservationDateTo(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "ReservationDateTo", x => x.ReservationDateTo.HasValue);
		}
		private PropertyMessage AnalyzeUnitTypeID(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "UnitTypeID", x => x.UnitTypeID > 0);
		}
		private PropertyMessage AnalyzeListIsClosed(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "ListIsClosed", x => x.ListIsClosed != null && x.ListIsClosed.Count > 0);
		}
		private PropertyMessage AnalyzeCountryID(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "CountryID", x => x.CountryID > 0);
		}
		private PropertyMessage AnalyzeCityID(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "CityID", x => x.CityID > 0);
		}
		private PropertyMessage AnalyzeSupplierID(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "SupplierID", x => x.SupplierID > 0);
		}
		private PropertyMessage AnalyzeCustomerID(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "CustomerID", x => x.CustomerID > 0);
		}
		private PropertyMessage AnalyzeUserID(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "UserID", x => x.UserID > 0);
		}
		private PropertyMessage AnalyzeSetReservationItemEmployeeIDs(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "SetReservationItemEmployeeIDs", x => x.SetReservationItemEmployeeIDs != null && x.SetReservationItemEmployeeIDs.Count > 0);
		}
		private PropertyMessage AnalyzeListServices(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "ListServices", x => x.ListServices != null && x.ListServices.Count > 0);
		}
		private PropertyMessage AnalyzeListSeasons(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "ListSeasons", x => x.ListSeasons != null && x.ListSeasons.Count > 0);
		}
		private PropertyMessage AnalyzeCustomFieldID(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "CustomFieldID", x => x.CustomFieldID > 0);
		}
		private PropertyMessage AnalyzeListDepartments(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "ListDepartments", x => x.ListDepartments != null && x.ListDepartments.Count > 0);
		}
		private PropertyMessage AnalyzeListGroupServices(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "ListGroupServices", x => x.ListGroupServices != null && x.ListGroupServices.Count > 0);
		}
		private PropertyMessage AnalyzeListCustomerTypes(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "ListCustomerTypes", x => x.ListCustomerTypes != null && x.ListCustomerTypes.Count > 0);
		}
		private PropertyMessage AnalyzeListUnitTypes(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "ListUnitTypes", x => x.ListUnitTypes != null && x.ListUnitTypes.Count > 0);
		}

		private PropertyMessage AnalyzeDepartmentLevel(IEnumerable<FinancialReportFilter> data)
		{
			var groupingOptions = data
				.Select(x => x.DepartmentLevel)
				.GroupBy(x => x)
				.Select(x => new
				{
					Key = x.Key.ToString(),
					Count = x.Count()
				})
				.ToDictionary(x => x.Key, x => x.Count);

			var response = new PropertyMessage
			{
				PropertyName = "DepartmentLevel",
				Values = groupingOptions
			};
			return response;
		}

		private PropertyMessage AnalyzeReservationInCurrency(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "ReservationsInCurrency", x => x.ReservationsInCurrency > 0);
		}

		private PropertyMessage AnalyzeSetPaid(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "SetPaidWithoutCommissionPaymentStatuses", x => x.SetPaidWithoutCommissionPaymentStatuses != null
				&& x.SetPaidWithoutCommissionPaymentStatuses.Count > 0);
		}

		private PropertyMessage AnalyzeReservationGrouping(IEnumerable<FinancialReportFilter> data)
		{
			return AnalyzeBoolean(data, "ReservationGrouping", x => x.ReservationGrouping);
		}

		private PropertyMessage AnalyzeBoolean(IEnumerable<FinancialReportFilter> data, string propertyName, Func<FinancialReportFilter, bool> test)
		{
			var satisfy = data.Count(test);
			var response = new PropertyMessage
			{
				PropertyName = propertyName
			};

			response.Values.Add("set", satisfy);
			response.Values.Add("not set", data.Count() - satisfy);

			return response;
		}

		private PropertyMessage AnalyzeGroupingOptions(IEnumerable<FinancialReportFilter> data)
		{
			var groupingOptions = data
				.Select(x => x.GroupingOptions)
				.GroupBy(x => x)
				.Select(x => new
				{
					Key = x.Key.ToString(),
					Count = x.Count()
				})
				.ToDictionary(x => x.Key, x => x.Count);

			var response = new PropertyMessage
			{
				PropertyName = "GroupingOptions",
				Values = groupingOptions
			};
			return response;
		}
	}

	class PropertyMessage
	{
		public string PropertyName { get; set; }
		public Dictionary<string, int> Values { get; set; }
		public PropertyMessage()
		{
			Values = new Dictionary<string, int>();
		}
	}
}
