﻿using System;
using System.Collections.Generic;

namespace KibanaViewStateAnalyzer.FinancialReportsAnalyzers.Services
{
	class FInancialReportLogFinder : LogFinder
	{
		private const string _LOG_FOLDER_NAME = "FinancialReportServiceFilter";
		private const int _PAGE_SIZE = 10000;
		public IEnumerable<EventLog> GetMessages(string instanceName, DateTime from, DateTime to)
		{
			var client = CreateClient();

			var response = new List<EventLog>();

			var skip = 0;
			var take = _PAGE_SIZE;
			var total = 0m;
			var results = client
				.Search<EventLog>(arg => 
					arg
						.Query(q => q
							.DateRange(d => d
								.Field(ev => ev.Time)
								.GreaterThanOrEquals(from)
								.LessThanOrEquals(to))
									&& q.Exists(e => e.Field(log => log.Message))
									&& q.Match(t => t.Field(f => f.LogFolderName).Query(_LOG_FOLDER_NAME))
									&& q.Match(t => t.Field(f => f.InstanceName).Query(instanceName))
						)
						.Skip(skip)
						.Take(take));
			response.AddRange(results.Documents);

			total = results.Total;
			skip = skip + take;

			return response;
		}
	}
}
