﻿namespace KibanaViewStateAnalyzer.FinancialReportsAnalyzers
{
	public enum PaidWithoutCommissionStatusEnum
	{
		Overpaid = 1,
		PaidFully = 2,
		PaidPartially = 3,
		Unpaid = 4
	}
}
