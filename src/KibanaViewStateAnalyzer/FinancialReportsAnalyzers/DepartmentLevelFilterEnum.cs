﻿namespace KibanaViewStateAnalyzer.FinancialReportsAnalyzers
{
	/// <summary>
	/// Defines level of department filtering
	/// </summary>
	public enum DepartmentLevelFilterEnum
	{
		/// <summary>
		/// Filter not set 
		/// </summary>
		NotSet = 0,

		/// <summary>
		/// Match Reservation
		/// </summary>
		Reservation = 1,

		/// <summary>
		/// Match reservation item 
		/// </summary>
		ReservationItem = 2,

		/// <summary>
		/// Match either reservation or reservation item 
		/// </summary>
		ReservationOrReservationItem = 3
	}
}
