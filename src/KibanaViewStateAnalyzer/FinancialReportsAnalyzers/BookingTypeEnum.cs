﻿namespace KibanaViewStateAnalyzer.FinancialReportsAnalyzers
{
	public enum BookingTypeEnum
	{
		/// <summary>
		/// Regular booking
		/// </summary>
		RegularBooking = 1,

		/// <summary>
		/// Operations booking
		/// </summary>
		OperationsBooking = 2,

		/// <summary>
		/// Cash advance
		/// </summary>
		CashAdvance = 3
	}
}
