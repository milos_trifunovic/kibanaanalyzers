﻿using System;
using System.Collections.Generic;

namespace KibanaViewStateAnalyzer.FinancialReportsAnalyzers
{
	class FinancialReportFilter
	{
		public const string AgencyEmployeeRoleName = "AgencijskiZaposlenik"; // for AspNet roles

		public FinancialReportGroupingOptions GroupingOptions { get; set; }

		// aditional grouping by reservation
		public bool ReservationGrouping { get; set; }
		public List<int> ListReservationStatuses { get; set; }
		/// <summary>
		/// Second level unit types
		/// </summary>
		public List<int> ListUnitTypes;
		public List<int> ListCustomerTypes { get; set; }
		public List<int> ListGroupServices { get; set; }
		public List<int> ListDepartments { get; set; }

		/// <summary>
		/// Determines how ListDepartments is used for filtering
		/// </summary>
		public DepartmentLevelFilterEnum DepartmentLevel { get; set; }

		public List<int> ListSeasons { get; set; }
		public List<int> ListServices { get; set; }
		public List<CustomFieldValueFilter> ReservationCustomFields { get; set; }
		public List<CustomFieldValueFilter> CustomerCustomFields { get; set; }
		/// <summary>
		/// List of custom fields to fetch from DB.
		/// </summary>
		public List<DodatnaPolja> CustomFieldsForFetching { get; set; }
		/// <summary>
		/// Set reservation item employee IDs 
		/// </summary>
		public HashSet<int> SetReservationItemEmployeeIDs { get; set; }
		/// <summary>
		/// Set reservation employee IDs
		/// </summary>
		public HashSet<int> SetReservationEmployeeIDs { get; set; }
		public List<int> ListBranchOffices { get; set; }
		/// <summary>
		/// Include data where service group not set
		/// </summary>
		public bool ServiceGroupNotSet { get; set; }
		public bool GroupCustomerPersonsAsOne { get; set; }
		public int CurrencyID { get; set; }
		public int LanguageID { get; set; }
		public int CustomFieldID { get; set; }
		/// <summary>
		/// User who made reservatin (Reservation.UserID), values with UserID > 0
		/// </summary>
		public int? UserID { get; set; }
		public int? CustomerID { get; set; }
		public int? SupplierID { get; set; }
		public int? CityID { get; set; }
		public int? CountryID { get; set; }

		/// <summary>
		/// If element in the list is true the reservation is closed with supplier invoice. 
		/// If element in the list is false then get all details that aren't closed with supplier invoice. 
		/// If element in the list is null the reservation's supplier invoice is not entered. 
		/// Don't filter if list is empty.
		/// </summary>
		public List<bool?> ListIsClosed { get; set; }

		/// <summary>
		/// First level unit type
		/// </summary>
		public int? UnitTypeID { get; set; }
		public DateTime? ReservationDateFrom { get; set; }
		public DateTime? ReservationDateTo { get; set; }
		public DateTime? InvoiceDateFrom { get; set; }
		public DateTime? InvoiceDateTo { get; set; }
		public DateTime? SupplierInvoiceDateFrom { get; set; }
		public DateTime? SupplierInvoiceDateTo { get; set; }
		/// <summary>
		/// This date only limits the WHERE clause of the query (meaning, it will just filter reservations. It will NOT limit the supplier invoices to these dates
		/// </summary>
		public DateTime? SupplierInvoiceExistsFrom { get; set; }
		/// <summary>
		/// This date only limits the WHERE clause of the query (meaning, it will just filter reservations. It will NOT limit the supplier invoices to these dates
		/// </summary>
		public DateTime? SupplierInvoiceExistsTo { get; set; }
		public DateTime? BookingDateFrom { get; set; }
		public DateTime? BookingDateTo { get; set; }
		// set invoice filtering as OR, InvoiceFilter OR SupplierInvoiceFilter
		public bool InvoiceFilteringAsOR { get; set; }
		/// <summary>
		/// ReservationItem start time (start value)
		/// </summary>
		public DateTime? StartPeriod { get; set; }
		/// <summary>
		/// ReservationItem start time (end value)
		/// </summary>
		public DateTime? EndPeriod { get; set; }
		/// <summary>
		/// (SELECT MIN(rezervacija_od) FROM rezervacije AS r WHERE r.sifra_velika_rezervacija =velike_rezervacije.sifra_velika_rezervacija ) >= @dateFirstCheckinFrom
		/// </summary>
		public DateTime? DateFirstCheckinFrom { get; set; }
		/// <summary>
		/// (SELECT MIN(rezervacija_od) FROM rezervacije AS r WHERE r.sifra_velika_rezervacija =velike_rezervacije.sifra_velika_rezervacija ) < @dateFirstCheckinTo "
		/// </summary>
		public DateTime? DateFirstCheckinTo { get; set; }
		/// <summary>
		/// (SELECT MAX(rezervacija_do) FROM rezervacije AS r WHERE r.sifra_velika_rezervacija =velike_rezervacije.sifra_velika_rezervacija ) >= @dateLastCheckoutFrom
		/// </summary>
		public DateTime? DateLastCheckoutFrom { get; set; }
		/// <summary>
		/// (SELECT MAX(rezervacija_do) FROM rezervacije AS r WHERE r.sifra_velika_rezervacija =velike_rezervacije.sifra_velika_rezervacija ) < @dateLastCheckoutTo 
		/// </summary>
		public DateTime? DateLastCheckoutTo { get; set; }
		/// <summary>
		/// Filters by specific reservation ID.
		/// </summary>
		public int? ReservationID { get; set; }
		/// <summary>
		/// Filters by VAT calculation types selected on the reservation.
		/// </summary>
		public List<int> ReservationVatCalculationTypes { get; set; }
		/// <summary>
		/// Filters by reservation item statuses
		/// </summary>
		public List<int> ListReservationItemStatuses { get; set; }
		/// <summary>
		/// If selected, only reservations in given currency will show up on the report
		/// </summary>
		public int ReservationsInCurrency { get; set; }
		/// <summary>
		/// List paid without commission payment statuses.
		/// </summary>
		public HashSet<PaidWithoutCommissionStatusEnum> SetPaidWithoutCommissionPaymentStatuses { get; set; }
		/// <summary>
		/// Reservation booking type, used to exclude ops bookings
		/// </summary>
		public BookingTypeEnum? ReservationBookingType { get; set; }

		/// <summary>
		/// Business entity IDs.
		/// </summary>
		public List<int> BusinessEntityIDs { get; set; }

		/// <summary>
		/// Use this property only in testing purposes to enforce legacy report retrieval, mostly used for comparing performances and for validating
		/// the retrieved results
		/// </summary>
		public bool EnforceEF { get; set; }
	}
}
