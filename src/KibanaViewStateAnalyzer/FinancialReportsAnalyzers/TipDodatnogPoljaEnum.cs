﻿namespace KibanaViewStateAnalyzer.FinancialReportsAnalyzers
{
	public enum TipDodatnogPoljaEnum
	{
		dropDownLista = 0,
		textBox = 1,
		textArea = 2,
		checkBox = 4,
		radEditor = 5,
		datePicker = 6
	}
}
