﻿namespace KibanaViewStateAnalyzer.FinancialReportsAnalyzers
{
	public enum FinancialReportGroupingOptions
	{
		ByDetail = 0,
		ByEmployee = 1,
		BySupplier = 2,
		ByCustomField = 3,
		ByServiceGroup = 4,
		ByService = 5,
		ByCity = 6,
		ByCountry = 7,
		ByDepartment = 8,
		ByBranchOffice = 9,
		ByObject = 10,
		ByCustomer = 11,
		ByCustomerCountry = 12,
		ByReservation = 13,
		ByReservationCreatedBy = 14,
		ByLeadAccommodationObject = 15,
		ByMonth = 16,
		ByLeadTour = 17
	}
}
