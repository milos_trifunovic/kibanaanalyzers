﻿using System;
using System.Collections.Generic;

namespace KibanaViewStateAnalyzer.FinancialReportsAnalyzers
{
	public class CustomFieldValueFilter
	{
		/// <summary>
		/// ID of a custom field
		/// </summary>
		public int CustomFieldID { get; set; }
		/// <summary>
		/// ID of a value inserted or selected in a custom  field
		/// </summary>
		public int CustomFieldValueID { get; set; }
		/// <summary>
		/// Multiple value IDs from the custom field filter (if multi select is enabled for that filter).
		/// <remarks>The list will contain a -1 value it the "Not set" item has been selected.</remarks>
		/// </summary>
		public List<int> CustomFieldValueIDs { get; set; }
		/// <summary>
		/// the value of custom field
		/// </summary>
		public string CustomFieldValue { get; set; }
		/// <summary>
		/// type of custom field (text box, date picker etc.)
		/// </summary>
		public TipDodatnogPoljaEnum CustomFieldType { get; set; }
		/// <summary>
		/// Affiliation of the custom field (reservation, document, customer etc.)
		/// </summary>
		public VrstaDodatnogPoljaEnum CustomFieldAffiliation { get; set; }
		/// <summary>
		/// Name of the custom field
		/// </summary>
		public string CustomFieldName { get; set; }
		/// <summary>
		/// in case of a datepicker type of custom field this a from value
		/// </summary>
		public DateTime? DateFromCustomFieldValue { get; set; }
		/// <summary>
		/// in case of a datepicker type of custom field this a to value
		/// </summary>
		public DateTime? DateToCustomFieldValue { get; set; }

		public const int DefaultDropDownListValue = -1;
		public override string ToString()
		{
			var s = string.Format("{0} (ID: {1}): {2}", CustomFieldName, CustomFieldID, CustomFieldValue);
			if (CustomFieldValueID > 0)
			{
				s = string.Format("{0} (IDVal: {1})", s, CustomFieldValueID);
			}

			return s;
		}
	}
}
