﻿namespace KibanaViewStateAnalyzer.FinancialReportsAnalyzers
{
	public enum VrstaDodatnogPoljaEnum
	{
		ReservationCustomFields = 1,
		CompanyCustomFields = 2,
		PassengersCustomFields = 3,
		CalculationItemCustomFields = 4,
		ReservationItemCustomFields = 5,
		DocumentsCustomFields = 6,
		PassengerReservationCustomFields = 7
	}
}
